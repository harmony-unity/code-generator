use std::collections::BTreeSet;
use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};

use crate::unity::Identifier;
use crate::util::{fs, path::PathExt, yaml};

/// Animator controllers collection.
///
/// Holds animator parameters and states.
#[derive(Debug, Default, Serialize)]
pub struct Animators {
    parameters: BTreeSet<Identifier>,
    states: BTreeSet<Identifier>,
}

impl Animators {
    /// Check if a file contains animator parameters or states definitions and adds them to the
    /// collection if so.
    ///
    /// A file may contain theses if the extension is `.controller`.
    pub async fn inspect<P: AsRef<Path>>(&mut self, path: P) {
        #[derive(Deserialize)]
        struct Document {
            #[serde(rename = "AnimatorController")]
            controller: Option<AnimatorController>,
            #[serde(rename = "AnimatorState")]
            state: Option<AnimatorState>,
        }

        #[derive(Deserialize)]
        struct AnimatorController {
            #[serde(rename = "m_AnimatorParameters")]
            parameters: Vec<AnimatorParameter>,
        }

        #[derive(Deserialize)]
        struct AnimatorParameter {
            #[serde(rename = "m_Name")]
            name: String,
        }

        #[derive(Deserialize)]
        struct AnimatorState {
            #[serde(rename = "m_Name")]
            name: String,
        }

        let path = path.as_ref();

        if path.extension_str() == "controller" {
            match fs::read(&path).await {
                Ok(file) => match yaml::from_str_all::<Document>(&file) {
                    Ok(docs) => for doc in docs {
                        if let Some(controller) = doc.controller {
                            for parameter in controller.parameters {
                                self.add_param(parameter.name, path);
                            }
                        } else if let Some(state) = doc.state {
                            self.add_state(state.name, path);
                        }
                    },
                    Err(e) => log::error!("Unable to parse \"{}\": {}.", path.display(), e),
                },
                Err(e) => log::error!("IO error reading \"{}\": {}.", path.display(), e),
            }
        }
    }

    fn add_param<P: Into<PathBuf>>(&mut self, parameter: String, path: P) {
        let parameter = Identifier::new(parameter, path);
        log::info!("Found animator parameter: {}", parameter.name());

        self.parameters.insert(parameter);
    }

    fn add_state<P: Into<PathBuf>>(&mut self, state: String, path: P) {
        let state = Identifier::new(state, path);
        log::info!("Found animator state: {}", state.name());

        self.states.insert(state);
    }
}