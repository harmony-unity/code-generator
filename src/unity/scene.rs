use std::collections::BTreeSet;
use std::path::Path;

use serde::Serialize;

use crate::unity::Asset;
use crate::util::path::PathExt;

/// Scene collection.
#[derive(Debug, Default, Serialize)]
#[serde(transparent)]
pub struct Scenes {
    scenes: BTreeSet<Asset>,
}

impl Scenes {
    /// Check if a file is a scene, and adds it to the collection if so.
    ///
    /// A file is a scene if the extension is `.unity`.
    pub async fn inspect<P: AsRef<Path>>(&mut self, path: P) {
        let path = path.as_ref();

        if path.extension_str() == "unity" {
            let scene = Asset::new(&path);
            log::info!("Found scene: {}", scene.name());

            if !self.scenes.insert(scene) {
                log::warn!("Duplicated scene found. Scenes should have distinct names. Otherwise, they may be difficult to load. See \"{}\".", path.display());
            }
        }
    }
}