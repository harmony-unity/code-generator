use std::io;
use std::path::Path;

/// IO Result extensions.
pub trait IoErrorExt {
    /// Improve the error message by appending the provided path to the error message.
    fn with_path<P: AsRef<Path>>(self, path: P) -> Self;
}

impl<T> IoErrorExt for io::Result<T> {
    fn with_path<P: AsRef<Path>>(self, path: P) -> Self {
        self.map_err(|e|
            io::Error::new(e.kind(), format!("{} : {}", e.to_string(), path.as_ref().display()))
        )
    }
}