#[deny(missing_debug_implementations)]
use std::process::ExitCode;

use args::Args;

mod app;
mod args;
mod log;
mod template;
mod unity;
mod util;

#[tokio::main]
async fn main() -> ExitCode {
    let args = Args::parse();
    log::init(&args);

    match app::run(&args).await {
        Ok(_) => ExitCode::SUCCESS,
        Err(e) => {
            ::log::error!("{:#}.", e);
            ExitCode::FAILURE
        }
    }
}