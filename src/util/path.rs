use std::path::{Path, PathBuf};

/// Extension methods for paths.
pub trait PathExt {
    /// File name as string.
    fn file_name_str(&self) -> &str;
    /// File stem as string.
    fn file_stem_str(&self) -> &str;
    /// File extension as string.
    fn extension_str(&self) -> &str;
}

impl PathExt for Path {
    fn file_name_str(&self) -> &str {
        self.file_name().unwrap_or_default().to_str().expect("path should be valid utf-8")
    }

    fn file_stem_str(&self) -> &str {
        self.file_stem().unwrap_or_default().to_str().expect("path should be valid utf-8")
    }

    fn extension_str(&self) -> &str {
        self.extension().unwrap_or_default().to_str().expect("path should be valid utf-8")
    }
}

impl PathExt for PathBuf {
    fn file_name_str(&self) -> &str {
        self.file_name().unwrap_or_default().to_str().expect("path should be valid utf-8")
    }

    fn file_stem_str(&self) -> &str {
        self.file_stem().unwrap_or_default().to_str().expect("path should be valid utf-8")
    }

    fn extension_str(&self) -> &str {
        self.extension().unwrap_or_default().to_str().expect("path should be valid utf-8")
    }
}