use std::cmp::Ordering;
use std::path::PathBuf;

use serde::Serialize;

use crate::util::path::PathExt;

/// Asset file.
///
/// Uniqueness is determined by the file name, not the path. This ensures that there will be no
/// identifier duplicates when generating the code.
#[derive(Debug, Serialize)]
pub struct Asset {
    name: String,
    path: PathBuf,
}

impl Asset {
    /// Create new asset from path.
    pub fn new<P: Into<PathBuf>>(path: P) -> Self {
        let path = path.into();
        Self {
            name: path.file_stem_str().to_owned(),
            path,
        }
    }

    /// Asset name (derived from file name).
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Eq for Asset {}

impl PartialEq for Asset {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Ord for Asset {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for Asset {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&other.name)
    }
}