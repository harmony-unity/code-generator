use std::collections::BTreeSet;
use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};

use crate::unity::Identifier;
use crate::util::{fs, path::PathExt, yaml};

/// Input action collection.
///
/// Holds control schemes, actions maps and actions.
#[derive(Debug, Default, Serialize)]
pub struct InputActions {
    control_schemes: BTreeSet<Identifier>,
    action_maps: BTreeSet<Identifier>,
    actions: BTreeSet<Identifier>,
}

impl InputActions {
    /// Check if a file contains control schemes, actions maps or actions definitions and adds them
    /// to the collection if so.
    ///
    /// A file may contain theses if the extension is `.inputactions`.
    pub async fn inspect<P: AsRef<Path>>(&mut self, path: P) {
        #[derive(Deserialize)]
        struct Document {
            #[serde(rename = "controlSchemes")]
            control_schemes: Vec<InputControlScheme>,
            #[serde(rename = "maps")]
            action_maps: Vec<InputActionMap>,
        }

        #[derive(Deserialize)]
        struct InputControlScheme {
            name: String,
        }

        #[derive(Deserialize)]
        struct InputActionMap {
            name: String,
            actions: Vec<InputAction>,
        }

        #[derive(Deserialize)]
        struct InputAction {
            name: String,
        }

        let path = path.as_ref();

        if path.extension_str() == "inputactions" {
            match fs::read(&path).await {
                Ok(file) => match yaml::from_str_all::<Document>(&file) {
                    Ok(docs) => for doc in docs {
                        for control_scheme in doc.control_schemes {
                            self.add_control_scheme(control_scheme.name, path);
                        }
                        for action_map in doc.action_maps {
                            self.add_action_map(action_map.name, path);
                            for action in action_map.actions {
                                self.add_action(action.name, path);
                            }
                        }
                    },
                    Err(e) => log::error!("Unable to parse \"{}\": {}.", path.display(), e),
                },
                Err(e) => log::error!("IO error reading \"{}\": {}.", path.display(), e),
            }
        }
    }

    fn add_control_scheme<P: Into<PathBuf>>(&mut self, control_scheme: String, path: P) {
        let control_scheme = Identifier::new(control_scheme, path);
        log::info!("Found input control scheme: {}", control_scheme.name());

        self.control_schemes.insert(control_scheme);
    }

    fn add_action_map<P: Into<PathBuf>>(&mut self, action_map: String, path: P) {
        let action_map = Identifier::new(action_map, path);
        log::info!("Found input action map: {}", action_map.name());

        self.action_maps.insert(action_map);
    }

    fn add_action<P: Into<PathBuf>>(&mut self, action: String, path: P) {
        let action = Identifier::new(action, path);
        log::info!("Found input action: {}", action.name());

        self.actions.insert(action);
    }
}