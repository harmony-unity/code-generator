use std::path::PathBuf;

use clap::Parser;

/// Generates C# classes of useful with information from a given Unity Project.
/// Generate and open them for further help about each file.
#[derive(Debug, Parser)]
#[command(name = "UnityCodeGenerator", author, version, about)]
pub struct Args {
    /// Path the the Unity project directory.
    #[arg(short, long)]
    pub input_dir: PathBuf,
    /// Path to the output directory. This directory will be created if not present.
    #[arg(short, long)]
    pub output_dir: PathBuf,
    /// Indicates if file paths should be included in generated code.
    #[arg(long)]
    pub generate_paths: bool,
    /// Starts the generator in unattended mode. This will disable text colors in terminal output.
    #[arg(short, long)]
    pub unattended: bool,
    /// Print detailed information about the code generation process.
    #[arg(short, long)]
    pub verbose: bool,
}

impl Args {
    /// Fetch and parse arguments.
    ///
    /// If arguments are incomplete or invalid, this shutdown the program and prints an help message.
    pub fn parse() -> Self {
        Parser::parse()
    }
}