use serde::Serialize;
use tera::{Context, Tera, Value};

use crate::unity::Project;

/// Code generator renderer.
///
/// Generate C# code from project assets and identifiers.
#[derive(Debug)]
pub struct Renderer {
    tera: Tera,
    args: RendererArgs,
}

#[derive(Debug, Serialize)]
struct RendererArgs {
    paths: bool,
}

impl Renderer {
    /// Create new renderer.
    pub fn new() -> Self {
        let mut tera = Tera::default();

        // Templates.
        tera.add_raw_templates([
            ("base", include_str!("./base.txt")),
            ("animator_parameters", include_str!("./animator_parameters.txt")),
            ("animator_states", include_str!("./animator_states.txt")),
            ("finder", include_str!("./finder.txt")),
            ("game_objects", include_str!("./game_objects.txt")),
            ("input_actions", include_str!("./input_actions.txt")),
            ("input_action_maps", include_str!("./input_action_maps.txt")),
            ("input_control_schemes", include_str!("./input_control_schemes.txt")),
            ("layers", include_str!("./layers.txt")),
            ("scenes", include_str!("./scenes.txt")),
            ("tags", include_str!("./tags.txt")),
        ]).expect("templates should have a valid syntax");

        // Testers.
        tera.register_tester("valid_identifier", Self::is_valid_identifier);

        Self {
            tera,
            args: RendererArgs {
                paths: false,
            },
        }
    }

    /// Indicates if file paths should be included in generated code.
    pub fn with_paths(mut self, paths: bool) -> Self {
        self.args.paths = paths;
        self
    }

    /// Render `AnimatorParameters.cs` from project.
    pub fn render_animator_parameters(&self, project: &Project) -> String {
        self.render("animator_parameters", &project)
    }

    /// Render `AnimatorStates.cs` from project.
    pub fn render_animator_states(&self, project: &Project) -> String {
        self.render("animator_states", &project)
    }

    /// Render `Finder.cs` from project.
    pub fn render_finder(&self, project: &Project) -> String {
        self.render("finder", &project)
    }

    /// Render `GameObjects.cs` from project.
    pub fn render_game_objects(&self, project: &Project) -> String {
        self.render("game_objects", &project)
    }

    /// Render `InputActions.cs` from project.
    pub fn render_input_actions(&self, project: &Project) -> String {
        self.render("input_actions", &project)
    }

    /// Render `InputActionMaps.cs` from project.
    pub fn render_input_action_maps(&self, project: &Project) -> String {
        self.render("input_action_maps", &project)
    }

    /// Render `InputControlSchemes.cs` from project.
    pub fn render_input_control_schemes(&self, project: &Project) -> String {
        self.render("input_control_schemes", &project)
    }

    /// Render `Layers.cs` from project.
    pub fn render_layers(&self, project: &Project) -> String {
        self.render("layers", &project)
    }

    /// Render `Scenes.cs` from project.
    pub fn render_scenes(&self, project: &Project) -> String {
        self.render("scenes", &project)
    }

    /// Render `Tags.cs` from project.
    pub fn render_tags(&self, project: &Project) -> String {
        self.render("tags", &project)
    }

    fn render(&self, template_name: &str, project: &Project) -> String {
        let mut context = Context::new();
        context.insert("args", &self.args);
        context.insert("project", &project);
        self.tera.render(template_name, &context).expect("template should generate correctly")
    }

    fn is_valid_identifier(value: Option<&Value>, _: &[Value]) -> tera::Result<bool> {
        let value = value.and_then(|it| it.as_str()).expect("identifier should exist and be a string");
        let mut chars = value.chars();

        Ok(
            //Len must be at least 1.
            !value.is_empty()
                //First character must be alphabetic.
                && chars.next().unwrap().is_ascii_alphabetic()
                //Next characters must be alphanumerical.
                && chars.all(|char| char.is_ascii_alphanumeric() || char == '_')
                //Any reserved keyword is prohibited.
                && !RESERVED_NAMES.contains(&value)
        )
    }
}

const RESERVED_NAMES: [&str; 79] = [
    "abstract",
    "bool",
    "continue",
    "decimal",
    "default",
    "event",
    "explicit",
    "extern",
    "char",
    "checked",
    "class",
    "const",
    "break",
    "as",
    "base",
    "delegate",
    "is",
    "lock",
    "long",
    "num",
    "byte",
    "case",
    "catch",
    "false",
    "finally",
    "fixed",
    "float",
    "for",
    "as",
    "foreach",
    "goto",
    "if",
    "implicit",
    "in",
    "int",
    "interface",
    "internal",
    "do",
    "double",
    "else",
    "namespace",
    "new",
    "null",
    "object",
    "operator",
    "out",
    "override",
    "params",
    "private",
    "protected",
    "public",
    "readonly",
    "sealed",
    "short",
    "sizeof",
    "ref",
    "return",
    "sbyte",
    "stackalloc",
    "static",
    "string",
    "struct",
    "void",
    "volatile",
    "while",
    "true",
    "try",
    "switch",
    "this",
    "throw",
    "unchecked",
    "unsafe",
    "ushort",
    "using",
    "virtual",
    "typeof",
    "uint",
    "ulong",
    "out"
];