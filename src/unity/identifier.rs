use std::cmp::Ordering;
use std::path::PathBuf;

use serde::Serialize;

/// Identifier found inside an asset. Theses can be tags, layers, C# classes, animation states, etc.
///
/// Uniqueness is determined by the name. This ensures that there will be no identifier duplicates
/// when generating the code.
#[derive(Debug, Serialize)]
pub struct Identifier {
    name: String,
    path: PathBuf,
}

impl Identifier {
    /// Create new identifier from name and path.
    pub fn new<S : Into<String>, P: Into<PathBuf>>(name: S, path: P) -> Self {
        Self {
            name: name.into(),
            path: path.into(),
        }
    }

    /// Identifier name.
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Eq for Identifier {}

impl PartialEq for Identifier {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Ord for Identifier {
    fn cmp(&self, other: &Self) -> Ordering {
        self.name.cmp(&other.name)
    }
}

impl PartialOrd for Identifier {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.name.partial_cmp(&other.name)
    }
}