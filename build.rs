fn main() {
    // Trigger recompilation when the Cargo.toml file is changed.
    // To ensure Clap about message always has up to date information about the binary.
    println!("cargo:rerun-if-changed=Cargo.toml");
}