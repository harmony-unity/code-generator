{% extends "base" %}

{% block name %}Tags{% endblock name %}

{% block content -%}
    {% for tag in project.settings.tags -%}
        {% if tag.name is valid_identifier -%}
            public const string {{tag.name}} = "{{tag.name}}";{% if args.paths %} // See "{{tag.path}}".{% endif %}
        {% else -%}
            // "{{tag.name}}" is invalid.{% if args.paths %} See "{{tag.path}}".{% endif %}
        {% endif -%}
    {% endfor -%}
{% endblock content -%}