use std::io;
use std::path::Path;

use futures::{pin_mut, StreamExt};
use serde::Serialize;

use crate::unity::{Animators, Findables, GameObjects, InputActions, Scenes, Settings};
use crate::util::fs;

/// Unity project.
///
/// This is essentially a collection of assets and identifiers found in those assets.
#[derive(Debug, Default, Serialize)]
pub struct Project {
    animators : Animators,
    findables : Findables,
    game_objects : GameObjects,
    input_actions : InputActions,
    scenes: Scenes,
    settings: Settings,
}

impl Project {
    /// Create new empty project.
    pub fn new() -> Self {
        Default::default()
    }

    /// Open an existing project from path.
    ///
    /// This can take a significant amount of time depending on the project size, as this has to
    /// walk the entire hierarchy of files and folders while also parsing most of the files.
    ///
    /// # Errors
    ///
    /// This will return an error if the path doesn't exist or isn't a directory. This will also do
    /// a basic check to ensure is this is an Unity project.
    pub async fn open<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let path = path.as_ref();

        // Ensure path leads to a Unity project.
        if !Self::is_project(&path).await? {
            return Err(io::Error::new(io::ErrorKind::Other, format!("\"{}\" doesn't seems to lead to a Unity project", path.display())));
        }

        // Inspect files for assets and identifiers.
        let mut project = Self::new();

        // Walk through assets files and settings.
        let assets = fs::walk_dir(path.join("Assets")).await?;
        let settings = fs::walk_dir(path.join("ProjectSettings")).await?;
        let files = assets.chain(settings);
        pin_mut!(files);

        while let Some(file) = files.next().await {
            match file {
                Ok(file) => {
                    futures::join!(
                        project.animators.inspect(&file),
                        project.findables.inspect(&file),
                        project.game_objects.inspect(&file),
                        project.input_actions.inspect(&file),
                        project.scenes.inspect(&file),
                        project.settings.inspect(&file),
                    );
                }
                Err(err) => {
                    log::error!("IO error: {}.", err);
                }
            }
        }
        Ok(project)
    }

    /// Check if the path contains a Unity Project by looking for the `Assets` and `ProjectSettings`
    /// directories.
    ///
    /// # Errors
    ///
    /// This will return an error if the path doesn't exist or isn't a directory.
    pub async fn is_project<P: AsRef<Path>>(path: P) -> io::Result<bool> {
        let path = path.as_ref();

        if !fs::exits(&path).await? {
            return Err(io::Error::new(io::ErrorKind::NotFound, "file not found"));
        }

        let asset_dir = path.join("Assets");
        let settings_dir = path.join("ProjectSettings");

        Ok(fs::exits(asset_dir).await? && fs::exits(settings_dir).await?)
    }
}