use std::cmp;
use regex::Regex;

/// Source code tokenizer.
pub trait Tokenizer<'a> {
    /// Produce a token. Advances the tokenizer using default delimiters.
    ///
    /// Returns none when tokenization is finished.
    fn next(&mut self) -> Option<&'a str>;

    /// Produce a token using a regex. Advances the tokenizer to the first regex match found.
    ///
    /// If a match is found, the tokenizer will produce a token using the resulting capture. It
    /// will then advance to the position after the match, not the capture. If no match is found,
    /// this advances the tokenizer to the end of the stream.
    ///
    /// Returns none when tokenization is finished.
    fn next_capture(&mut self, regex: &Regex, capture: usize) -> Option<&'a str>;

    /// Produce a token using a set of opening and closing delimiters. The produced token will
    /// correspond to the content within these delimiters.
    ///
    /// If the provided delimiters are not found, this advances the tokenizer to the very end of the
    /// stream.
    ///
    /// Returns none when tokenization is finished.
    fn next_block(&mut self, start: char, end: char) -> Option<&'a str>;

    /// Returns a reference to the next token without advancing the tokenizer.
    ///
    /// See [Tokenizer::next].
    fn peek(&mut self) -> Option<&'a str>;

    /// Advances the tokenizer to the next token, skipping the current one.
    ///
    /// See [Tokenizer::next].
    fn skip(&mut self);

    /// Advances the tokenizer to the next line.
    fn skip_line(&mut self);

    /// Advances the tokenizer after a specific pattern.
    fn skip_after(&mut self, pattern: &str);

    /// Create a tokenizer that skips code comments.
    ///
    /// Skips comments single line comments `//` and multiline comments `/* */`.
    fn skip_comments(self) -> SkipComments<Self>
        where Self: Sized {
        SkipComments::new(self)
    }
}

/// C# tokenizer.
#[derive(Debug)]
pub struct CSharpTokenizer<'a> {
    remaining: &'a str,
}

impl<'a> CSharpTokenizer<'a> {
    /// Create a new C# tokenizer for the provided content.
    pub fn new(content: &'a str) -> Self {
        Self {
            remaining: content.trim(),
        }
    }

    /// Find position of the next token, using default delimiters.
    ///
    /// This doesn't advance the tokenizer.
    fn find(&self) -> usize {
        let mut position = self.remaining.find(Self::is_whitespace_or_delimiter).unwrap_or_else(|| self.remaining.len());
        if position == 0 && self.remaining.starts_with(Self::is_delimiter) {
            //Include delimiters in tokens
            //If starts with a delimiter, length is at least one
            position = 1
        }
        position
    }

    /// Extract a token starting at current position and ending at provided position.
    ///
    /// This doesn't advance the tokenizer.
    ///
    /// # Panics
    ///
    /// Panics if position is out of range.
    fn tokenize(&self, position: usize) -> Option<&'a str> {
        let token = &self.remaining[..position];
        if !token.is_empty() { Some(token) } else { None }
    }

    /// Advances the tokenizer to the provided position.
    ///
    /// # Panics
    ///
    /// Panics if position is out of range.
    fn advance(&mut self, position: usize) {
        self.remaining = &self.remaining[position..].trim_start();
    }

    /// Advances the tokenizer by looking for a specific character.
    ///
    /// New position will be set after the character, if found. Otherwise, it lands at the very end
    /// of the stream.
    fn advance_after_character(&mut self, character: char) {
        let mut position = self.remaining.find(character).unwrap_or_else(|| self.remaining.len());
        position = Self::next_char(&self.remaining, position);
        self.remaining = &self.remaining[position..].trim_start();
    }

    /// Advances the tokenizer by looking for a specific string.
    ///
    /// New position will be set after the pattern, if found. Otherwise, it lands at the very end
    /// of the stream.
    fn advance_after_pattern(&mut self, pattern: &str) {
        let mut position = self.remaining.find(pattern).unwrap_or_else(|| self.remaining.len());
        position = Self::next_char(&self.remaining, position);
        self.remaining = &self.remaining[position..].trim_start();
    }

    /// Indicates if a character is considered a whitespace character.
    fn is_whitespace(character: char) -> bool {
        character.is_ascii_whitespace()
    }

    /// Indicates if a character is considered a delimiter.
    fn is_delimiter(character: char) -> bool {
        character == ';'
            || character == ':'
            || character == '.'
            || character == '{'
            || character == '}'
            || character == '<'
            || character == '>'
            || character == '('
            || character == ')'
            || character == '['
            || character == ']'
    }

    /// Indicates if a character is considered a whitespace character or a delimiter.
    fn is_whitespace_or_delimiter(character: char) -> bool {
        Self::is_whitespace(character) || Self::is_delimiter(character)
    }

    /// Return the position of the next char, as UTF-8 characters may span multiple bytes.
    fn next_char(content: &str, position: usize) -> usize {
        // Make sure to not overflow.
        if position >= content.len() { return content.len(); }

        // Find the next char boundary.
        let mut next_position = position + 1;
        while !content.is_char_boundary(next_position) {
            next_position += 1;
        }
        next_position
    }
}

impl<'a> Tokenizer<'a> for CSharpTokenizer<'a> {
    fn next(&mut self) -> Option<&'a str> {
        let position = self.find();
        let token = self.tokenize(position);
        self.advance(position);

        token
    }

    fn next_capture(&mut self, regex: &Regex, capture: usize) -> Option<&'a str> {
        if let Some(captures) = regex.captures(self.remaining) {
            if let Some(capture) = captures.get(capture) {
                let token = &self.remaining[capture.range()];
                self.advance(captures.get(0).expect("matches should have at least one capture").end());
                return Some(token);
            }
        }
        self.advance(self.remaining.len());
        None
    }

    fn next_block(&mut self, start: char, end: char) -> Option<&'a str> {
        let block_start = self.remaining.find(start).unwrap_or_else(|| self.remaining.len());

        let mut recursions = 0usize;
        let search_start = cmp::min(block_start + 1, self.remaining.len());
        let mut block_end = self.remaining[search_start..].chars().enumerate().find(|(_, it)| {
            if *it == start {
                recursions = recursions + 1;
                false
            } else if *it == end {
                if recursions > 0 {
                    recursions = recursions - 1;
                    false
                } else {
                    true
                }
            } else {
                false
            }
        }).map(|(i, _)| block_start + i + 1).unwrap_or_else(|| self.remaining.len());
        block_end = Self::next_char(&self.remaining, block_end);

        let block = &self.remaining[block_start..block_end];
        self.advance(block_end);

        if !block.is_empty() { Some(block) } else { None }
    }

    fn peek(&mut self) -> Option<&'a str> {
        let position = self.find();
        let token = self.tokenize(position);

        token
    }

    fn skip(&mut self) {
        self.advance(self.find());
    }

    fn skip_line(&mut self) {
        self.advance_after_character('\n')
    }

    fn skip_after(&mut self, pattern: &str) {
        self.advance_after_pattern(pattern)
    }
}

/// Tokenizer that skips comments.
///
/// Skips comments single line comments `//` and multiline comments `/* */`.
#[derive(Debug)]
pub struct SkipComments<T> {
    tokenizer: T,
}

impl<'a, T> SkipComments<T>
    where T: Tokenizer<'a> {
    /// Create a new tokenizer that skip comments, wrapping the provided one.
    fn new(tokenizer: T) -> Self {
        Self {
            tokenizer
        }
    }

    /// Advances tokenizer, skipping comments.
    fn skip_comments(&mut self) {
        loop {
            let token = self.tokenizer.peek();
            match token {
                Some(token) => {
                    if token.starts_with("//") {
                        self.tokenizer.skip();
                        self.tokenizer.skip_line();
                    } else if token.starts_with("/*") {
                        self.tokenizer.skip();
                        if !token.ends_with("*/") {
                            self.tokenizer.skip_after("*/");
                        }
                    } else {
                        break;
                    }
                }
                None => {
                    break;
                }
            }
        }
    }
}

impl<'a, T> Tokenizer<'a> for SkipComments<T>
    where T: Tokenizer<'a> {
    fn next(&mut self) -> Option<&'a str> {
        self.skip_comments();
        self.tokenizer.next()
    }

    fn next_capture(&mut self, regex: &Regex, capture: usize) -> Option<&'a str> {
        self.skip_comments();
        self.tokenizer.next_capture(regex, capture)
    }

    fn next_block(&mut self, start: char, end: char) -> Option<&'a str> {
        self.skip_comments();
        self.tokenizer.next_block(start, end)
    }

    fn peek(&mut self) -> Option<&'a str> {
        self.skip_comments();
        self.tokenizer.peek()
    }

    fn skip(&mut self) {
        self.skip_comments();
        self.tokenizer.skip()
    }

    fn skip_line(&mut self) {
        self.skip_comments();
        self.tokenizer.skip_line()
    }

    fn skip_after(&mut self, pattern: &str) {
        self.skip_comments();
        self.tokenizer.skip_after(pattern)
    }
}