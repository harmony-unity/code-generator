use lazy_static::lazy_static;
use regex::Regex;
use serde::de::DeserializeOwned;

/// Deserialize an instance of type `T` from a string of YAML text.
pub fn from_str<T: DeserializeOwned>(
    value: &str
) -> serde_yaml::Result<T> {
    serde_yaml::from_str(&value)
}

/// Deserialize multiple instances of type `T` from a string of YAML text.
pub fn from_str_all<T: DeserializeOwned>(
    value: &str
) -> serde_yaml::Result<Vec<T>> {
    // Identify YAML directives.
    const DIRECTIVE_INDICATOR: char = '%';

    lazy_static! {
        //Identifies YAML document separators.
        static ref DOCUMENT_SEPARATOR : Regex = Regex::new(r"---.*\n").unwrap();
    }

    DOCUMENT_SEPARATOR
        .split(&value)
        .filter(|it| !it.starts_with(DIRECTIVE_INDICATOR))
        .map(from_str)
        .collect()
}