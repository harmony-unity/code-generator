use std::cmp::Ordering;
use std::collections::BTreeSet;
use std::path::{Path, PathBuf};

use lazy_static::lazy_static;
use regex::Regex;
use serde::Serialize;

use crate::util::{csharp::{self, Tokenizer}, fs, path::PathExt};

/// Findable class.
///
/// Uniqueness is determined by the fully qualified name of the class.
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Findable {
    namespace: String,
    name: String,
    tag: String,
    path: PathBuf,
}

impl Findable {
    /// Create new findable from namespace, name, tag and path.
    pub fn new<P: Into<PathBuf>>(namespace: &str, name: &str, tag: &str, path: P) -> Self {
        Self {
            namespace: namespace.to_owned(),
            name: name.to_owned(),
            tag: tag.to_owned(),
            path: path.into(),
        }
    }

    /// Findable name.
    pub fn name(&self) -> &str {
        &self.name
    }
}

impl Eq for Findable {}

impl PartialEq for Findable {
    fn eq(&self, other: &Self) -> bool {
        self.namespace == self.namespace && self.name == other.name
    }
}

impl Ord for Findable {
    fn cmp(&self, other: &Self) -> Ordering {
        (&self.namespace, &self.name).cmp(&(&other.namespace, &other.name))
    }
}

impl PartialOrd for Findable {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        (&self.namespace, &self.name).partial_cmp(&(&other.namespace, &other.name))
    }
}

/// Findable collection.
#[derive(Debug, Default, Serialize)]
#[serde(transparent)]
pub struct Findables {
    findables: BTreeSet<Findable>,
}

impl Findables {
    /// Check if a file contains findable definitions and adds them to the collection if so.
    ///
    /// A file may contain a findable if the extension is `.cs`.
    pub async fn inspect<P: AsRef<Path>>(&mut self, path: P) {
        let path = path.as_ref();

        if path.extension_str() == "cs" {
            match fs::read(&path).await {
                Ok(file) => for findable in Self::parse(&file, "", &path) {
                    log::info!("Found findable: {}", findable.name());
                    self.findables.insert(findable);
                },
                Err(e) => log::error!("IO error reading \"{}\": {}.", path.display(), e),
            }
        }
    }

    fn parse(file: &str, namespace: &str, path: &Path) -> Vec<Findable> {
        lazy_static! {
            //Captures fully qualified namespace name.
            static ref NAMESPACE : Regex = Regex::new(r"^((?:\w+\s*\.?\s*)*)").unwrap();
            //Captures fully qualified class name.
            static ref CLASS : Regex = Regex::new(r"^(?:\[.*?\]\s*)*(?:\w+\s)*class\s+(\w+):?").unwrap();
            //Captures Findable attribute tag.
            static ref FINDABLE : Regex = Regex::new(r#"^Findable\(("?(?:\w+\.?)*"?)\)\]"#).unwrap();
        }

        let mut findables = Vec::new();
        let mut tokenizer = csharp::CSharpTokenizer::new(file).skip_comments();
        while let Some(token) = tokenizer.next() {
            if token == "namespace" {
                if let Some(name) = tokenizer.next_capture(&NAMESPACE, 1) {
                    if let Some(block) = tokenizer.next_block('{', '}') {
                        let name = vec![namespace, name].into_iter().filter(|it| !it.is_empty()).collect::<Vec<&str>>().join(".");
                        findables.append(&mut Self::parse(block, name.trim(), path));
                    }
                }
            } else if token == "[" {
                if let Some(tag) = tokenizer.next_capture(&FINDABLE, 1) {
                    if let Some(name) = tokenizer.next_capture(&CLASS, 1) {
                        findables.push(Findable::new(namespace, name, tag, path));
                    }
                }
            }
        }
        findables
    }
}