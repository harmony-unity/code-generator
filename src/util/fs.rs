use std::path::{Path, PathBuf};

use async_stream::try_stream;
use futures::Stream;
use tokio::{fs, io};

use crate::util::io::IoErrorExt;

/// Read the entire contents of a file into a string.
///
/// # Errors
///
/// This will return an error if the file doesn't exist or couldn't be read.
pub async fn read<P: AsRef<Path>>(
    path: P
) -> io::Result<String> {
    fs::read_to_string(&path).await.with_path(&path)
}

/// Write the entire contents of a file using a string.
///
/// # Errors
///
/// This will return an error if the file couldn't be created or written to.
pub async fn write<P: AsRef<Path>>(
    path: P,
    content: &str,
) -> io::Result<()> {
    fs::write(&path, &content).await.with_path(&path)
}

/// Check if a file or a directory exists.
///
/// # Errors
///
/// This will return an error if the file or directory couldn't be read.
pub async fn exits<P: AsRef<Path>>(
    path: P
) -> io::Result<bool> {
    match fs::metadata(path).await {
        Ok(_) => Ok(true),
        Err(e) if e.kind() == io::ErrorKind::NotFound => Ok(false),
        Err(e) => Err(e)
    }
}

/// Returns a stream over the entries within a directory and their childrens.
///
/// # Errors
///
/// This will *immediately* return an error if the directory doesn't exist or couldn't be read.
/// Then, each subsequent file directory can individually produce an error, whilst not preventing
/// any further traversal of the hierarchy.
pub async fn walk_dir<P: AsRef<Path>>(
    path: P
) -> io::Result<impl Stream<Item=io::Result<PathBuf>>> {
    let path = path.as_ref();

    // First dir to visit.
    let dir = fs::read_dir(&path).await.with_path(&path)?;

    // Dirs to visit.
    let mut dirs = vec![(dir, path.to_owned())];

    Ok(try_stream! {
        // While there is dirs to visit.
        while let Some((mut dir, path)) = dirs.pop() {
            while let Some(entry) = dir.next_entry().await.with_path(&path)? {
                let path = entry.path();
                if entry.file_type().await.with_path(&path)?.is_dir() {
                    dirs.push((fs::read_dir(&path).await.with_path(&path)?, path));
                } else {
                    yield path;
                }
            }
        }
    })
}

/// Recursively creates a directory and all of its parent components if they are missing.
///
/// # Errors
///
/// This will return an error if any of the required directory could not be created.
pub async fn create_dir_all<P: AsRef<Path>>(
    path: P
) -> io::Result<()> {
    fs::create_dir_all(&path).await.with_path(&path)
}