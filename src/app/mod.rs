use std::time::Instant;

use anyhow::Context;

use crate::args::Args;
use crate::template::Renderer;
use crate::unity::Project;
use crate::util::fs;

/// Run the application.
pub async fn run(args: &Args) -> anyhow::Result<()> {
    // To keep track of process duration
    let start_time = Instant::now();

    // Open project.
    log::info!("Opening project at \"{}\".", &args.input_dir.display());
    let project = Project::open(&args.input_dir).await.context("Unable to open project")?;

    // Create output directory if it doesn't exist.
    log::info!("Creating output directory at \"{}\".", &args.output_dir.display());
    fs::create_dir_all(&args.output_dir).await.context("Unable to create output directory")?;

    // Generate and write code.
    let renderer = Renderer::new().with_paths(args.generate_paths);
    let templates: [(&str, fn(&Renderer, &Project) -> String); 10] = [
        ("AnimatorParameters.cs", |renderer, project| renderer.render_animator_parameters(&project)),
        ("AnimatorStates.cs", |renderer, project| renderer.render_animator_states(&project)),
        ("Finder.cs", |renderer, project| renderer.render_finder(&project)),
        ("GameObjects.cs", |renderer, project| renderer.render_game_objects(&project)),
        ("InputActions.cs", |renderer, project| renderer.render_input_actions(&project)),
        ("InputActionMaps.cs", |renderer, project| renderer.render_input_action_maps(&project)),
        ("InputControlSchemes.cs", |renderer, project| renderer.render_input_control_schemes(&project)),
        ("Layers.cs", |renderer, project| renderer.render_layers(&project)),
        ("Scenes.cs", |renderer, project| renderer.render_scenes(&project)),
        ("Tags.cs", |renderer, project| renderer.render_tags(&project)),
    ];
    for (name, template) in templates {
        log::info!("Generating \"{}\".", name);
        fs::write(
            &args.output_dir.join(&name),
            &template(&renderer, &project),
        ).await.with_context(|| format!("Unable to write \"{}\"", &name))?;
    }

    // Show process duration.
    let duration = start_time.elapsed();
    log::info!("Generation took {} milliseconds.", duration.as_millis());

    Ok(())
}

