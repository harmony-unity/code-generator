use crate::args::Args;

/// Initialize application logger.
///
/// By default, prints error and warn messages. Setting it to verbose also prints info messages and
/// enables timestamps.
pub fn init(args: &Args) {
    simplelog::TermLogger::init(
        if args.verbose { log::LevelFilter::Info } else { log::LevelFilter::Warn },
        simplelog::ConfigBuilder::new()
            .set_location_level(log::LevelFilter::Debug)
            .set_target_level(log::LevelFilter::Debug)
            .set_thread_level(log::LevelFilter::Debug)
            .set_time_level(log::LevelFilter::Info)
            .build(),
        simplelog::TerminalMode::Mixed,
        if args.unattended { simplelog::ColorChoice::Never } else { simplelog::ColorChoice::Auto },
    ).expect("logger should not be set already");
}