use std::collections::BTreeSet;
use std::path::{Path, PathBuf};

use serde::{Deserialize, Serialize};

use crate::unity::Identifier;
use crate::util::{fs, yaml, path::PathExt};

/// GameObject collection.
#[derive(Debug, Default, Serialize)]
#[serde(transparent)]
pub struct GameObjects {
    game_objects: BTreeSet<Identifier>,
}

impl GameObjects {
    /// Check if a file contains game object definitions and adds them to the collection if so.
    ///
    /// A file may contain GameObjects if the extension is `.unity` or `.prefab`.
    pub async fn inspect<P: AsRef<Path>>(&mut self, path: P) {
        #[derive(Deserialize)]
        struct Document {
            #[serde(rename = "GameObject")]
            game_object: Option<GameObject>,
            #[serde(rename = "PrefabInstance")]
            prefab_instance: Option<PrefabInstance>,
        }

        #[derive(Deserialize)]
        struct GameObject {
            #[serde(rename = "m_Name")]
            name: Option<String>,
        }

        #[derive(Deserialize)]
        struct PrefabInstance {
            #[serde(rename = "m_Modification")]
            modifications: PrefabModifications,
        }

        #[derive(Deserialize)]
        struct PrefabModifications {
            #[serde(rename = "m_Modifications")]
            modifications: Vec<PrefabModification>,
        }

        #[derive(Debug, Deserialize)]
        struct PrefabModification {
            #[serde(rename = "propertyPath")]
            path: String,
            value: String,
        }

        let path = path.as_ref();
        let extension = path.extension_str();

        if extension == "unity" || extension == "prefab" {
            match fs::read(&path).await {
                Ok(file) => match yaml::from_str_all::<Document>(&file) {
                    Ok(docs) => for doc in docs {
                        if let Some(name) = doc.game_object.and_then(|it| it.name) {
                            self.add_game_object(name, path);
                        } else if let Some(prefab_instance) = doc.prefab_instance {
                            for name in prefab_instance.modifications.modifications.into_iter()
                                .filter(|it| it.path == "m_Name")
                                .map(|it| it.value) {
                                self.add_game_object(name, path);
                            }
                        }
                    },
                    Err(e) => log::error!("Unable to parse \"{}\": {}.", path.display(), e),
                },
                Err(e) => log::error!("IO error reading \"{}\": {}.", path.display(), e),
            }
        }
    }

    fn add_game_object<P: Into<PathBuf>>(&mut self, game_object: String, path: P) {
        let game_object = Identifier::new(game_object, path);
        log::info!("Found game object: {}", game_object.name());

        self.game_objects.insert(game_object);
    }
}