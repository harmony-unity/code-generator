use std::collections::BTreeSet;
use std::path::Path;

use serde::{Deserialize, Serialize};

use crate::unity::Identifier;
use crate::util::{fs, yaml, path::PathExt};

/// Project settings collection.
///
/// Holds tags and layers.
#[derive(Debug, Default, Serialize)]
pub struct Settings {
    tags: BTreeSet<Identifier>,
    layers: BTreeSet<Identifier>,
}

impl Settings {
    /// Check if a file has tags or layers definitions, and adds them to this collection if so.
    ///
    /// Looks for a file named `TagManager.asset` folder.
    pub async fn inspect<P: AsRef<Path>>(&mut self, path: P) {
        #[derive(Deserialize)]
        struct Document {
            #[serde(rename = "TagManager")]
            tag_manager: TagManager,
        }

        #[derive(Deserialize)]
        struct TagManager {
            tags: Vec<String>,
            layers: Vec<String>,
        }

        const TAG_MANAGER_FILE_NAME: &str = "TagManager.asset";

        let path = path.as_ref();

        if path.file_name_str() == TAG_MANAGER_FILE_NAME {
            if !self.tags.is_empty() {
                log::warn!("Multiple \"{}\" files found. This isn't normal. See \"{}\".", TAG_MANAGER_FILE_NAME, path.display());
            }

            match fs::read(&path).await {
                Ok(file) => {
                    match yaml::from_str::<Document>(&file) {
                        Ok(file) => {
                            let TagManager { tags, layers } = file.tag_manager;

                            //Default tags are not in the asset file.
                            self.tags.insert(Identifier::new("Untagged", &path));
                            self.tags.insert(Identifier::new("Respawn", &path));
                            self.tags.insert(Identifier::new("Finish", &path));
                            self.tags.insert(Identifier::new("EditorOnly", &path));
                            self.tags.insert(Identifier::new("MainCamera", &path));
                            self.tags.insert(Identifier::new("Player", &path));
                            self.tags.insert(Identifier::new("GameController", &path));

                            // Tags.
                            for tag in tags.into_iter().filter(|it| !it.is_empty()) {
                                let tag = Identifier::new(&tag, &path);
                                log::info!("Found tag: {}", tag.name());

                                if !self.tags.insert(tag) {
                                    log::warn!("Duplicated tag found. See \"{}\".", path.display());
                                }
                            }

                            // Layers.
                            for layer in layers.into_iter().filter(|it| !it.is_empty()) {
                                let layer = Identifier::new(&layer, &path);
                                log::info!("Found layer: {}.", layer.name());

                                if !self.layers.insert(layer) {
                                    log::warn!("Duplicated layer found. See \"{}\".", path.display());
                                }
                            }
                        }
                        Err(e) => log::error!("Unable to parse \"{}\": {}.", path.display(), e),
                    }
                }
                Err(e) => log::error!("IO error reading \"{}\": {}.", path.display(), e),
            }
        }
    }
}