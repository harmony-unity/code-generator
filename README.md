<div align="center">

![Icon](.docs/icon.svg)
# Code Generator

</div>

This is the Harmony project *Code Generator*. This is a command line utility that reads your Unity project and generates
various useful static classes.

* **GameObjects** : Game object names.
* **Tags** : Tags.
* **Layers** : Layers.
* **Scenes** : Scene names.
* **AnimatorParameters** : Animator parameter hashes.
* **AnimatorStates** : Animator state names.
* **InputControlSchemes** : Input control scheme names.
* **InputActionMaps** : Input action map names.
* **InputActions** : Input action names.
* **Finder** : Service provider for each `MonoBehaviour` annotated with `[Findable("Your Tag")]`.

## Usage

### Running the tool

This is a command line tool. As input, provide the path to your Unity project folder. As output, provide the path
where the generated files should go.

```text
code-generator --input-dir "/path/to/your/awesome-project" --output-dir "/path/to/your/awesome-project/Assets/Generated"
```

See the `--help` for more information.

```text
Generates C# classes of useful with information from a given Unity Project. Generate and open them for further help 
about each file

Usage: code-generator.exe [OPTIONS] --input-dir <INPUT_DIR> --output-dir <OUTPUT_DIR>

Options:
  -i, --input-dir <INPUT_DIR>    Path the the Unity project directory
  -o, --output-dir <OUTPUT_DIR>  Path to the output directory. This directory will be created if not present
  -v, --verbose                  Print detailed information about the code generation process
  -h, --help                     Print help
  -V, --version                  Print version
```

### Using the generated code

All generated code can be found inside the `Harmony` namespace. Simply access what you need in a static way.

```csharp
// Import the namespace.
using Harmony;

// Here is an example loading a scene by name.
SceneManager.LoadScene(Scenes.HUD, LoadSceneMode.Additive);
```

### Using findables

The `Finder` is a static class providing references to component singletons. It is especially useful to fetch components
without having to make a call to `GameObject.FindWithTag` followed by `GetComponent`, which is very error-prone. This is
also pretty slow if this component is required by a lot of objects. 

```csharp
// Mannually.
MusicPlayer musicPlayer = GameObject.FindWithTag("GameMusic")?.GetComponent<MusicPlayer>();

// Using the finder.
MusicPlayer musicPlayer = Finder.MusicPlayer;
```

To make a *findable* component, add it to a tagged *GameObject* in your scene. 

![Adding a component to a *GameObject*](.docs/findable-gameobject.png)

Then, inside the *C#* script, put the attribute `[Findable]` above the class definition, including the tag used. Note
that you should use the `Tags` class.

```csharp
[Findable(Tags.Music)]
public class MusicPlayer : MonoBehaviour
{
    /* ... */
}
```

Once this is done, save all your files (Scripts, Scenes, Prefabs, ...) and run the code generator. You should find a new
property for your findable inside the `Finder` class.

## Motivation

Tags and Layers are frequently used in the project, sometimes directly in the code. Having to write them each time
is not only time-consuming, but very error-prone. What if the tag is renamed or removed ? One can manually write a 
*const* class for this, but then you have to maintain it, which can be cumbersome.
  
This is where this tool comes in : this generates automatically the const classes. When a tag is renamed or removed,
the CSharp compiler can notify you. This means less errors and less maintenance.

## Why Rust ?

This tool is now at his 7th iteration :

* **The "Python Era"** (V1-V2) : First version was writen in *Python*. The code was short and sweet, but it was 
painfully slow for big projects. We've seen it run for 20 minutes sometimes. Ouch. I've tried to speed things up with a 
*multi-process* version, but it took 100% of CPU for 5 minutes. A big improvement, but still too much.
* **The "C# Version"** (V3): Very short lived version. Writen in C#, it was *WAY* faster and much much cleaner. Took 
about 1 minute to generate the files. Unfortunately, the Yaml parser was using a lot of memory (about 2 Go) when 
reading large scene files.
* **The "Go Version"** (V4): A student of mine, while learning Go, tried to make his own version. Using the same project 
that took 20 minutes for the Python version, he managed to get it down to 6 seconds. We used his version for a while.
* **The "Rust Era"** (V5-V6-V7): Having just learned *Rust*, I wondered if I could use it for this tool. First version 
was very rough...but *man* was it fast. Using our "20 minutes benchmark project", it only took 620 milliseconds to read
and write the files. Then, I improved the design, making it more maintainable, which lead to the 6th version that had
progress bars. This 7th version now uses the new *async* runtime. It's a bit slower than the first version (about
2 seconds) because of the more robust handling of files. Nonetheless, this is a *way* easier version to work with.

This is where we are now. *Rust* was a happy accident while prototyping, and this is why we're using it.

## FAQ

### Why can't I use spaces in *GameObject* names ? You could replace them with underscores.

The code generator uses the exact *GameObject* name for the constant name. For example, a *GameObject* 
named `plAyeR` will generate this constant.

```csharp
public static partial class GameObjects
{
    public const string plAyeR = "plAyeR";
}
```
We do not *camelize*/modify the names in any way, and here's why. Let's say you want your *GameObject* name to 
be `My Plant` instead. We *could*, indeed, replace the spaces with underscores, like this :

```csharp
public static partial class GameObjects
{
    public const string My_Plant = "My Plant";
}
```

But what if we had an other *GameObject* named `My_Plant` ? In that case, there would be two constants with the same
name : one for `My Plant` and one for `My_Plant`.

```csharp
public static partial class GameObjects
{
    public const string My_Plant = "My Plant";
    public const string My_Plant = "My_Plant";
}
```

Again, we *could* use an escape strategy, like doubling the underscores. Unfortunately, this opens the door to other
escaping requests. *Could you escape parentheses ? Could you escape slashes ?* It would be a never ending hell of 
escaping tricks. Thus, we've decided to not alter the constant names to fit them in the language constraints.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details